<?php

namespace App\Models\Slugs;

use A17\Twill\Models\Model;

class TeamMemberSlug extends Model
{
    protected $table = "team_member_slugs";
}
