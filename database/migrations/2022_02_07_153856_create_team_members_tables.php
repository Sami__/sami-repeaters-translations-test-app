<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTeamMembersTables extends Migration
{
    public function up()
    {
        Schema::create('team_members', function (Blueprint $table) {
            // this will create an id, a "published" column, and soft delete and timestamps columns
            createDefaultTableFields($table);
            $table->integer('position')->unsigned()->nullable();

            $table->foreignId('team_id')
                ->constrained()
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });

        Schema::create('team_member_translations', function (Blueprint $table) {
            createDefaultTranslationsTableFields($table, 'team_member');
            $table->string('title', 200)->nullable();
            $table->text('description')->nullable();
        });

        Schema::create('team_member_slugs', function (Blueprint $table) {
            createDefaultSlugsTableFields($table, 'team_member');
        });


    }

    public function down()
    {

        Schema::dropIfExists('team_member_translations');
        Schema::dropIfExists('team_member_slugs');
        Schema::dropIfExists('team_members');
    }
}
