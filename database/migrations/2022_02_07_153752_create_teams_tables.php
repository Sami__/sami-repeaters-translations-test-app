<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTeamsTables extends Migration
{
    public function up()
    {
        Schema::create('teams', function (Blueprint $table) {
            // this will create an id, a "published" column, and soft delete and timestamps columns
            createDefaultTableFields($table);
        });

        Schema::create('team_translations', function (Blueprint $table) {
            createDefaultTranslationsTableFields($table, 'team');
            $table->string('title', 200)->nullable();
            $table->text('description')->nullable();
        });

        Schema::create('team_slugs', function (Blueprint $table) {
            createDefaultSlugsTableFields($table, 'team');
        });


    }

    public function down()
    {

        Schema::dropIfExists('team_translations');
        Schema::dropIfExists('team_slugs');
        Schema::dropIfExists('teams');
    }
}
