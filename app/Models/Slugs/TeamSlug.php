<?php

namespace App\Models\Slugs;

use A17\Twill\Models\Model;

class TeamSlug extends Model
{
    protected $table = "team_slugs";
}
