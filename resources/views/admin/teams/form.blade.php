@extends('twill::layouts.form')

@section('contentFields')
    @formField('repeater', ['type' => 'team-member'])
@stop
