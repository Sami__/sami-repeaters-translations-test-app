<?php

namespace App\Repositories;

use A17\Twill\Repositories\Behaviors\HandleTranslations;
use A17\Twill\Repositories\Behaviors\HandleSlugs;
use A17\Twill\Repositories\ModuleRepository;
use App\Models\Team;

class TeamRepository extends ModuleRepository
{
    use HandleTranslations, HandleSlugs;

    public function __construct(Team $model)
    {
        $this->model = $model;
    }


    public function afterSave($object, $fields)
    {
        $this->updateRepeater($object, $fields, 'members', 'TeamMember', 'team-member');
        parent::afterSave($object, $fields);
    }


    public function getFormFields($object)
    {
        $fields = parent::getFormFields($object);
        $fields = $this->getFormFieldsForRepeater($object, $fields, 'members', 'TeamMember', 'team-member');
        return $fields;
    }
}
