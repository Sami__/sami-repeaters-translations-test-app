<?php

namespace App\Http\Controllers\Admin;

use A17\Twill\Http\Controllers\Admin\ModuleController as BaseModuleController;

class TeamController extends BaseModuleController
{
    protected $moduleName = 'teams';

    protected $indexOptions = [
    ];
}
