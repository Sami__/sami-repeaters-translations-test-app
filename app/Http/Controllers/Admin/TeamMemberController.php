<?php

namespace App\Http\Controllers\Admin;

use A17\Twill\Http\Controllers\Admin\ModuleController as BaseModuleController;

class TeamMemberController extends BaseModuleController
{
    protected $moduleName = 'teamMembers';

    protected $indexOptions = [
    ];
}
